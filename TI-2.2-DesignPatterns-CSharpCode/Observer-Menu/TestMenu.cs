﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer_Menu
{
    class TestMenu
    {
        public static void TestMenus()
        {
            Recepten r = new Recepten();
            r.VoegToe(new Pannenkoek("Kaaspannenkoek", 5.90));
            KinderMenu km = new KinderMenu(r);
            r.VoegToe(new Pannenkoek("Appelpannenkoek met stroop", 4.50));
            OnlineMenu om = new OnlineMenu(r);
            r.VoegToe(new Pannenkoek("Spekpannenkoek met stroop", 6.40));
            r.Verwijder(0);
            r.VerwijderMenu(km);
            r.VoegToe(new Pannenkoek("Appel-spek-uipannenkoek met tomaat", 8.50));
        }
    }
}
