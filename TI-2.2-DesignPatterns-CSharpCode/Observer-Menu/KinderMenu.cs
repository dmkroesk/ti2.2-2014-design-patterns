﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer_Menu
{
    class KinderMenu : ObserverMenu
    {
        private Recepten receptenSubject;

        public KinderMenu(Recepten recepten)
        {
            receptenSubject = recepten;
            receptenSubject.RegistreerMenu(this);
        }

        public void Actualiseer()
        {
            Console.WriteLine("Actualiseer Kindermenu...");
            DrukMenuAf();
        }

        public void DrukMenuAf()
        {
            List<Pannenkoek> pannenkoekenLijst;
            receptenSubject.GeefMenuGegevens(out pannenkoekenLijst);
            Console.WriteLine("\nKindermenu:");
            foreach (Pannenkoek p in pannenkoekenLijst)
            {
                Console.WriteLine("{0} : {1}", p.Beschrijving(), p.Prijs());
            }
            Console.WriteLine();
        }
    }
}
