﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer_Menu
{
    class Recepten : MenuGegevens
    {
        private List<ObserverMenu> observers;
        private List<Pannenkoek> pannenkoeken;

        public Recepten()
        {
            observers = new List<ObserverMenu>();
            pannenkoeken = new List<Pannenkoek>();
        }

        public void RegistreerMenu(ObserverMenu menu)
        {
            Console.WriteLine("Registreer menu observer");
            observers.Add(menu);
        }

        public void VerwijderMenu(ObserverMenu menu)
        {
            if (observers.Contains(menu))
            {
                Console.WriteLine("Verwijder menu observer");
                observers.Remove(menu);
            }
        }

        public void VerwittigMenus()
        {
            Console.WriteLine("Verwittig alle menu observers");
            foreach (ObserverMenu menu in observers)
            {
                menu.Actualiseer();
            }
            Console.WriteLine("Alle menu observers zijn verwittigd");
        }

        public void VoegToe(Pannenkoek pannenkoek)
        {
            pannenkoeken.Add(pannenkoek);
            VerwittigMenus();
        }

        public void Verwijder(int itemNr)
        {
            if (itemNr < pannenkoeken.Count)
            {
                pannenkoeken.RemoveAt(itemNr);
                VerwittigMenus();
            }
        }

        public void GeefMenuGegevens(out List<Pannenkoek> menuGegevens)
        {
            menuGegevens = pannenkoeken;
        }
    }
}
