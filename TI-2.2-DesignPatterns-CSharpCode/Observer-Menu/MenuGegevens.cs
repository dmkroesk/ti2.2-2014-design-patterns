﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer_Menu
{
    interface MenuGegevens
    {
        void RegistreerMenu(ObserverMenu menu);
        void VerwijderMenu(ObserverMenu menu);
        void VerwittigMenus();
    }
}
