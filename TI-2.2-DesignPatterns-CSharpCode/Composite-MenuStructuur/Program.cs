﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_MenuStructuur
{
    class Program
    {
        static void Main(string[] args)
        {
            TestMenuStructuur.TestMenu();

            Console.WriteLine("Druk op Enter...");
            Console.ReadLine(); // Wacht tot de gebruiker op Enter drukt
        }
    }
}
