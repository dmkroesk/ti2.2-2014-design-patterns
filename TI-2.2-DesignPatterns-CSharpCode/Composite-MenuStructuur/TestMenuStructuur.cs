﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_MenuStructuur
{
    class TestMenuStructuur
    {
        public static void TestMenu()
        {
            MenuComponent menu = new Menu("Pannenkoeken");
            // Eerst een gewoon niet-genest menu
            menu.VoegToe(new MenuItem(new Pannenkoek("Kaaspannenkoek", 4.75)));
            menu.VoegToe(new MenuItem(new Pannenkoek("Naturelpannenkoek met stroop", 4.25)));
            menu.VoegToe(new MenuItem(new Pannenkoek("Spekpannenkoek met stroop", 6.00)));
            menu.DrukAf();
            Console.WriteLine();
            // Uitbreiding met een submenu
            Menu kinderMenu = new Menu("Kindermenu");
            kinderMenu.VoegToe(new MenuItem(new Pannenkoek("Naturelpannenkoek met suiker", 3.50)));
            kinderMenu.VoegToe(new MenuItem(new Pannenkoek("Appelpannenkoek met stroop", 4.15)));
            menu.VoegToe(kinderMenu);
            menu.DrukAf();
            Console.WriteLine();
            // Uitbreiding met een item in het hoofdmenu
            menu.VoegToe(new MenuItem(new Pannenkoek("Kaas-spekpannenkoek met champignons", 7.75)));
            menu.DrukAf();
            Console.WriteLine();
            // Uitbreiding met een sub-submenu
            Menu snelMenu = new Menu("Snelmenu");
            snelMenu.VoegToe(new MenuItem(new Pannenkoek("Naturelpannenkoek met hagelslag", 3.25)));
            kinderMenu.VoegToe(snelMenu);
            menu.DrukAf();
            Console.WriteLine();
        }
    }
}
