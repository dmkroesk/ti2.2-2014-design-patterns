﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_MenuStructuur
{
    abstract class MenuComponent
    {
        public abstract void DrukAf(); // Implementeer in afgeleide klassen

        public virtual void VoegToe(MenuComponent component)
        {
            // Default gedrag is leeg, override in Menu klasse
        }

        public virtual void Verwijder(int componentNr)
        {
            // Default gedrag is leeg, override in Menu klasse
        }

        public virtual MenuComponent GeefSubItem(int itemNr)
        {
            // Default gedrag is leeg, override in Menu klasse
            throw new NotImplementedException();
        }
    }
}
