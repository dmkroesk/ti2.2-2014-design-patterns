﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_MenuStructuur
{
    class MenuItem : MenuComponent
    {
        private Pannenkoek pannenkoek;

        public MenuItem(Pannenkoek p)
        {
            pannenkoek = p;
        }

        public override void DrukAf()
        {
            Console.WriteLine("{0,40} {1,5:F2}", pannenkoek.Beschrijving(), pannenkoek.Prijs());
        }
    }
}
