﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_MenuStructuur
{
    class Pannenkoek
    {
        private string beschrijving;
        private double prijs;

        public Pannenkoek(string beschr, double pr)
        {
            beschrijving = beschr;
            prijs = pr;
        }

        public string Beschrijving()
        {
            return beschrijving;
        }

        public double Prijs()
        {
            return prijs;
        }
    }
}
