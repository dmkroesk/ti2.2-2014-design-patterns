﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite_MenuStructuur
{
    class Menu : MenuComponent
    {
        private string naam;
        private List<MenuComponent> menuItems;

        public Menu(string menuNaam)
        {
            naam = menuNaam;
            menuItems = new List<MenuComponent>();
        }

        public override void VoegToe(MenuComponent item)
        {
            menuItems.Add(item);
        }

        public override void Verwijder(int componentNr)
        {
            menuItems.RemoveAt(componentNr);
        }

        public override MenuComponent GeefSubItem(int itemNr)
        {
            return menuItems.ElementAt(itemNr);
        }

        public override void DrukAf()
        {
            Console.WriteLine("-- {0} --", naam);
            foreach (MenuComponent item in menuItems)
            {
                item.DrukAf();
            }
            Console.WriteLine();
        }
    }
}
