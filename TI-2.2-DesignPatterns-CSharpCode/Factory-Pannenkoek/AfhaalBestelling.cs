﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator_Pannenkoek;

namespace Factory_Pannenkoek
{
    class AfhaalBestelling : Bestelling
    {
        protected override Pannenkoek MaakPannenkoek(PannenkoekSoorten soortPannenkoek)
        {
            switch (soortPannenkoek)
            {
                case PannenkoekSoorten.Kaas:
                    return new KaasPannenkoek();
                case PannenkoekSoorten.KaasExtra:
                    return new Champignons(new KaasPannenkoek());
                case PannenkoekSoorten.Stroop:
                    return new Stroop(new NaturelPannenkoek());
                case PannenkoekSoorten.Normaal:
                default:
                    return new NaturelPannenkoek();
            }
        }

        public override void LeverAf()
        {
            Console.WriteLine("Afhaalbestelling: leg klaar voor afhalen door de klant");
        }
    }
}
