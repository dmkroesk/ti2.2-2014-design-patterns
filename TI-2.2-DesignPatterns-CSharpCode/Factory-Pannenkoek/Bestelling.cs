﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator_Pannenkoek; // Gebruik de Pannenkoek klassen uit deze namespace

namespace Factory_Pannenkoek
{
    abstract class Bestelling
    {
        public enum PannenkoekSoorten
        {
            Normaal, Stroop, Kaas, KaasExtra
        }

        private List<Pannenkoek> besteldePannenkoeken;

        /// <summary>
        /// Factory method, te implementeren door afgeleide klassen
        /// </summary>
        /// <param name="soortPannenkoek">Soort pannenkoek om te instantiëren</param>
        /// <returns></returns>
        protected abstract Pannenkoek MaakPannenkoek(PannenkoekSoorten soortPannenkoek);

        /// <summary>
        /// Afleveren van bestelde pannenkoeken is specifiek voor de aard van
        /// de bestelling, dus te implementeren door afgeleide klassen
        /// </summary>
        public abstract void LeverAf();

        public Bestelling()
        {
            besteldePannenkoeken = new List<Pannenkoek>();
        }

        public void VoegToe(PannenkoekSoorten soortPannenkoek)
        {
            besteldePannenkoeken.Add(MaakPannenkoek(soortPannenkoek));
        }

        public void DrukAf()
        {
            Console.WriteLine("--- Bestelling ---");
            foreach (Pannenkoek p in besteldePannenkoeken)
            {
                Console.WriteLine(p.Beschrijving());
            }
            Console.WriteLine("------------------");
        }

        public void MaakKlaar()
        {
            foreach (Pannenkoek p in besteldePannenkoeken)
            {
                Console.WriteLine("Maak nu {0} klaar", p.Beschrijving());
            }
        }
    }
}
