﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory_Pannenkoek
{
    class TestBestelling
    {
        public static void TestBestelling1()
        {
            Console.WriteLine("\n>>> Test OnlineBestelling");
            Bestelling onlineBestelling = new OnlineBestelling();
            VulBestelling(onlineBestelling);
            VerwerkBestelling(onlineBestelling);
            Console.WriteLine("\n>>> Test AfhaalBestelling");
            Bestelling afhaalBestelling = new AfhaalBestelling();
            VulBestelling(afhaalBestelling);
            VerwerkBestelling(afhaalBestelling);
        }

        private static void VulBestelling(Bestelling b)
        {
            b.VoegToe(Bestelling.PannenkoekSoorten.Normaal);
            b.VoegToe(Bestelling.PannenkoekSoorten.Kaas);
            b.VoegToe(Bestelling.PannenkoekSoorten.KaasExtra);
            b.VoegToe(Bestelling.PannenkoekSoorten.Stroop);
        }

        private static void VerwerkBestelling(Bestelling b)
        {
            b.DrukAf();
            b.MaakKlaar();
            b.LeverAf();
        }
    }
}
