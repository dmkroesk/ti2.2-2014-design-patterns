﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator_Pannenkoek;

namespace Factory_Pannenkoek
{
    class OnlineBestelling : Bestelling
    {
        protected override Pannenkoek MaakPannenkoek(PannenkoekSoorten soortPannenkoek)
        {
            // Niet alle soorten zijn geschikt voor verzending, dus handel af
            // wat mogelijk is, en lever de default pannenkoek voor alle andere bestellingen
            switch (soortPannenkoek)
            {
                case PannenkoekSoorten.Kaas:
                    return new KaasPannenkoek();
                case PannenkoekSoorten.KaasExtra:
                    return new Champignons(new KaasPannenkoek());
                case PannenkoekSoorten.Normaal:
                default:
                    return new NaturelPannenkoek();
            }
        }

        public override void LeverAf()
        {
            Console.WriteLine("Online bestelling: verzend de bestelling naar de klant");
        }
    }
}
