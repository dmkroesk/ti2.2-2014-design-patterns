﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Kassabon
{
    class KortingNul : KortingCalculator
    {
        public KortingNul() { }

        public double Korting() { return 0.0; }
    }
}
