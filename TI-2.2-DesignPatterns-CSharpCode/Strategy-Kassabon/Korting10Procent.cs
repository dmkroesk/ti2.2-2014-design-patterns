﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Kassabon
{
    class Korting10Procent : KortingCalculator
    {
        private Kassabon kassabon;

        public Korting10Procent(Kassabon kassabon)
        {
            this.kassabon = kassabon;
        }

        public double Korting()
        {
            return kassabon.Totaalprijs() * 0.1;
        }
    }
}
