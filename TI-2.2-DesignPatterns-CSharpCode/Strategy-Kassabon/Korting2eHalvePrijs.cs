﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Kassabon
{
    class Korting2eHalvePrijs : KortingCalculator
    {
        private Kassabon kassabon;

        public Korting2eHalvePrijs(Kassabon kassabon)
        {
            this.kassabon = kassabon;
        }

        public double Korting()
        {
            if (kassabon.VraagOp(1) != null)
            {
                return kassabon.VraagOp(1).Prijs() / 2;
            }
            else
            {
                return 0.0;
            }
        }
    }
}
