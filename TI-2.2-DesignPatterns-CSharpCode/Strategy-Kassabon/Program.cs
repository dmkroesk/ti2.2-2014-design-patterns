﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Kassabon
{
    class Program
    {
        static void Main(string[] args)
        {
            TestKassabon.TestKassabon1();
            TestKassabon.TestKassabon2();

            Console.WriteLine("Druk op Enter...");
            Console.ReadLine(); // Wacht tot de gebruiker op Enter drukt
        }
    }
}
