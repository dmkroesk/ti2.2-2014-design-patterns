﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    class TestPannenkoek
    {
        public static void TestPannenkoek1()
        {
            Pannenkoek p = new Stroop(new KaasPannenkoek());
            ToonPannenkoek(p);
        }

        public static void TestPannenkoek2()
        {
            Pannenkoek p = new Stroop(new Stroop(new NaturelPannenkoek()));
            ToonPannenkoek(p);
        }

        public static void TestPannenkoek3()
        {
            Pannenkoek p = new Champignons(new Stroop(new KaasPannenkoek()));
            ToonPannenkoek(p);
        }

        public static void TestPannenkoek4()
        {
            Pannenkoek p = new KaasPannenkoek();
            ToonPannenkoek(p);
        }

        public static void TestPannenkoek5()
        {
            Pannenkoek kaaspannenkoek = new KaasPannenkoek();
            Pannenkoek champignons = new Champignons(kaaspannenkoek);
            Pannenkoek stroop = new Stroop(champignons);
            ToonPannenkoek(kaaspannenkoek);
            ToonPannenkoek(champignons);
            ToonPannenkoek(stroop);
        }

        public static void ToonPannenkoek(Pannenkoek p)
        {
            Console.WriteLine("Pannenkoekbeschrijving = {0}, Prijs = {1}", p.Beschrijving(), p.Prijs());
        }
    }
}
