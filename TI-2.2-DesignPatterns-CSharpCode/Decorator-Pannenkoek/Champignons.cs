﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    public class Champignons : PannenkoekDecorator
    {
        private Pannenkoek pannenkoek;
        private static double champignonsPrijs = 0.80;
        private static string champignonsBeschrijving = " + Champignons";

        public Champignons(Pannenkoek teDecorerenPannenkoek)
        {
            pannenkoek = teDecorerenPannenkoek;
        }

        /// <summary>
        /// Geeft de beschrijving van de gedecoreerde pannenkoek en voegt daaraan
        /// de champignons toe
        /// </summary>
        /// <returns></returns>
        public override string Beschrijving()
        {
            //Console.WriteLine("Champignons.Beschrijving() aangeroepen");
            return pannenkoek.Beschrijving() + champignonsBeschrijving;
        }

        /// <summary>
        /// Geeft de prijs van de gedecoreerde pannenkoek plus die van
        /// de champignons
        /// </summary>
        /// <returns></returns>
        public override double Prijs()
        {
            //Console.WriteLine("Champignons.Prijs() aangeroepen");
            return pannenkoek.Prijs() + champignonsPrijs;
        }
    }
}
