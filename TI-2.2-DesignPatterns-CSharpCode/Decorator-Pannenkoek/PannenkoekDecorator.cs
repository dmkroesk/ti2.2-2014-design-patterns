﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    /// <summary>
    /// Met deze klasse worden ingredienten toegevoegd aan een pannenkoek.
    /// Merk op dat door het opstapelen van een aantal van deze wrapper klassen
    /// net zoveel ingredienten als gewenst kunnen worden toegevoegd.
    /// </summary>
    public abstract class PannenkoekDecorator : Pannenkoek
    {
        public abstract override string Beschrijving(); // Dwing de afgeleide klassen om Beschrijving opnieuw te implementeren
    }
}
