﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    public class KaasPannenkoek : Pannenkoek
    {
        private static double kaasPannenkoekPrijs = 4.75;
        private static string kaasPannenkoekBeschrijving = "Kaaspannenkoek";

        public KaasPannenkoek()
        {
            beschrijving = kaasPannenkoekBeschrijving;
        }

        public override double Prijs()
        {
            //Console.WriteLine("KaasPannenkoek.Prijs() aangeroepen");
            return kaasPannenkoekPrijs;
        }
    }
}
