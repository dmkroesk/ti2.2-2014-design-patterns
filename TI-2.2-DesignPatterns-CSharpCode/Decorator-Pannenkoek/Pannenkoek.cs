﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    /// <summary>
    /// De basisklasse voor een pannenkoek.
    /// Hieraan kunnen met decorators ingredienten worden toegevoegd.
    /// </summary>
    public abstract class Pannenkoek
    {
        protected string beschrijving = "Onbekende pannenkoek";
        
        /// <summary>
        /// Geeft de beschrijving van de pannenkoek
        /// </summary>
        /// <returns></returns>
        public virtual string Beschrijving()
        {
            //Console.WriteLine("Pannenkoek.Beschrijving() aangeroepen");
            return beschrijving;
        }

        /// <summary>
        /// Geeft de prijs van de pannenkoek (bijvoorbeeld in Euro)
        /// </summary>
        /// <returns></returns>
        public abstract double Prijs();
    }
}
