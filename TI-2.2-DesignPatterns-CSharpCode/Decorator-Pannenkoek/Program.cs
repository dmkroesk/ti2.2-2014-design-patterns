﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestPannenkoek.TestPannenkoek1();
            //TestPannenkoek.TestPannenkoek2();
            //TestPannenkoek.TestPannenkoek3();
            //TestPannenkoek.TestPannenkoek4();
            TestPannenkoek.TestPannenkoek5();

            Console.WriteLine("Druk op Enter...");
            Console.ReadLine(); // Wacht tot de gebruiker op Enter drukt
        }
    }
}
