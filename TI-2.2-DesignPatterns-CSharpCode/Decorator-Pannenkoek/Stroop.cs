﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    public class Stroop : PannenkoekDecorator
    {
        private Pannenkoek pannenkoek;
        private static double stroopPrijs = 0.50;
        private static string stroopBeschrijving = " + Stroop";

        public Stroop(Pannenkoek teDecorerenPannenkoek)
        {
            pannenkoek = teDecorerenPannenkoek;
        }

        /// <summary>
        /// Geeft de beschrijving van de gedecoreerde pannenkoek en voegt daaraan
        /// de stroop toe
        /// </summary>
        /// <returns></returns>
        public override string Beschrijving()
        {
            //Console.WriteLine("Stroop.Beschrijving() aangeroepen");
            return pannenkoek.Beschrijving() + stroopBeschrijving;
        }

        /// <summary>
        /// Geeft de prijs van de gedecoreerde pannenkoek plus die van
        /// de stroop
        /// </summary>
        /// <returns></returns>
        public override double Prijs()
        {
            //Console.WriteLine("Stroop.Prijs() aangeroepen");
            return pannenkoek.Prijs() + stroopPrijs;
        }
    }
}
