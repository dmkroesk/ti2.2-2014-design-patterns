﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Pannenkoek
{
    public class NaturelPannenkoek : Pannenkoek
    {
        private static double naturelPannenkoekPrijs = 3.60;
        private static string naturelPannenkoekBeschrijving = "Naturel pannenkoek";

        public NaturelPannenkoek()
        {
            beschrijving = naturelPannenkoekBeschrijving;
        }

        public override double Prijs()
        {
            //Console.WriteLine("NaturelPannenkoek.Prijs() aangeroepen");
            return naturelPannenkoekPrijs;
        }
    }
}
