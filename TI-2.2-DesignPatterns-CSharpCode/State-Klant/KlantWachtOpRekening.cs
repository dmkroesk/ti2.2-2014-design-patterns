﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class KlantWachtOpRekening : KlantStatus
    {
        public Klant.Wens DeelWensMee(Klant klant)
        {
            Console.WriteLine("Klant: geen wens (wil nog steeds rekening)");
            return Klant.Wens.Rekening;
        }

        public void BrengMenukaart(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt menukaart in status {0}", klant.VraagStatus());
        }

        public void BrengBestelling(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt bestelling in status {0}", klant.VraagStatus());
        }

        public void BrengRekening(Klant klant)
        {
            Console.WriteLine("Klant: ontvangt en betaalt rekening, en vertrekt");
            klant.WijzigStatus(new KlantVertrekt());
        }
    }
}
