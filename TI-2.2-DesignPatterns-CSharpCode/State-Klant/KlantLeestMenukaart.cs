﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class KlantLeestMenukaart : KlantStatus
    {
        private Random toevalsgetal;

        public KlantLeestMenukaart()
        {
            toevalsgetal = new Random();
        }

        public Klant.Wens DeelWensMee(Klant klant)
        {
            Klant.Wens wens = Klant.Wens.Niets;
            if (KeuzeGemaakt())
            {
                Console.WriteLine("Klant: heeft keuze gemaakt");
                klant.WijzigStatus(new KlantWachtOpBestelling());
                wens = KiesBestelling();
            }
            else
            {
                Console.WriteLine("Klant: is nog niet klaar met menukaart lezen");
            }
            return wens;
        }

        public void BrengMenukaart(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt menukaart in status {0}", klant.VraagStatus());
        }

        public void BrengBestelling(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt bestelling in status {0}", klant.VraagStatus());
        }

        public void BrengRekening(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt rekening in status {0}", klant.VraagStatus());
        }

        protected virtual bool KeuzeGemaakt()
        {
            return (toevalsgetal.NextDouble() > 0.5);
        }

        protected virtual Klant.Wens KiesBestelling()
        {
            if (toevalsgetal.NextDouble() < 0.5)
            {
                return Klant.Wens.Koffie;
            }
            else
            {
                return Klant.Wens.Pannenkoek;
            }
        }
    }
}
