﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    /// <summary>
    /// Klant klasse met gebruik van het State design pattern
    /// </summary>
    class Klant
    {
        public enum Wens { Niets, Menukaart, Koffie, Pannenkoek, Rekening };

        private KlantStatus status;
        private Random toevalsgetal;

        public Klant()
        {
            toevalsgetal = new Random();
            status = new KlantWachtOpMenukaart();
        }

        public KlantStatus VraagStatus()
        {
            return status;
        }

        public void WijzigStatus(KlantStatus nieuweStatus)
        {
            status = nieuweStatus;
        }

        public Wens DeelWensMee()
        {
            return status.DeelWensMee(this);
        }

        public void BrengMenukaart()
        {
            status.BrengMenukaart(this);
        }

        public void BrengBestelling()
        {
            status.BrengBestelling(this);
        }

        public void BrengRekening()
        {
            status.BrengRekening(this);
        }
    }
}
