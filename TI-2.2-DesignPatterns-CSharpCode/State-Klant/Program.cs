﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class Program
    {
        static void Main(string[] args)
        {
            TestKlant.TestKlantZonderStatePattern();

            TestKlant.TestKlantMetStatePattern();

            Console.WriteLine("\nDruk op Enter om af te sluiten...");
            Console.ReadLine();
        }
    }
}
