﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class KlantConsumeertBestelling : KlantStatus
    {
        private Random toevalsgetal;

        public KlantConsumeertBestelling()
        {
            toevalsgetal = new Random();
        }

        public Klant.Wens DeelWensMee(Klant klant)
        {
            Klant.Wens wens = Klant.Wens.Niets;
            if (BestellingGeconsumeerd())
            {
                if (WilAfrekenen())
                {
                    Console.WriteLine("Klant: wil afrekenen en vraagt de rekening");
                    klant.WijzigStatus(new KlantWachtOpRekening());
                    wens = Klant.Wens.Rekening;
                }
                else
                {
                    Console.WriteLine("Klant: wil nog iets bestellen");
                    klant.WijzigStatus(new KlantWachtOpMenukaart());
                    wens = Klant.Wens.Menukaart;
                }
            }
            else
            {
                Console.WriteLine("Klant: is nog niet klaar met eten/drinken");
            }

            return wens;
        }

        public void BrengMenukaart(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt menukaart in status {0}", klant.VraagStatus());
        }

        public void BrengBestelling(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt bestelling in status {0}", klant.VraagStatus());
        }

        public void BrengRekening(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt rekening in status {0}", klant.VraagStatus());
        }

        protected virtual bool BestellingGeconsumeerd()
        {
            return (toevalsgetal.NextDouble() > 0.6);
        }

        protected virtual bool WilAfrekenen()
        {
            return (toevalsgetal.NextDouble() > 0.4);
        }
    }
}
