﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class Serveerster
    {
        public bool ServeerKlantZonderStatePattern(KlantZonderStatePattern klant)
        {
            bool klantVertrekt = false;
            KlantZonderStatePattern.Wens wens = klant.DeelWensMee();
            switch (wens)
            {
                case KlantZonderStatePattern.Wens.Menukaart:
                    Console.WriteLine("Serveerster: brengt klant gevraagde menukaart");
                    klant.BrengMenukaart();
                    break;

                case KlantZonderStatePattern.Wens.Koffie:
                case KlantZonderStatePattern.Wens.Pannenkoek:
                    Console.WriteLine("Serveerster: neemt bestelling op, brengt daarna klant gevraagde bestelling");
                    klant.BrengBestelling();
                    break;

                case KlantZonderStatePattern.Wens.Rekening:
                    Console.WriteLine("Serveerster: brengt klant gevraagde rekening");
                    klant.BrengRekening();
                    klantVertrekt = true;
                    break;

                case KlantZonderStatePattern.Wens.Niets:
                default:
                    Console.WriteLine("Serveerster: klant heeft geen wens");
                    break;
            }
            return !klantVertrekt;
        }

        public bool ServeerKlant(Klant klant)
        {
            bool klantVertrekt = false;
            Klant.Wens wens = klant.DeelWensMee();
            switch (wens)
            {
                case Klant.Wens.Menukaart:
                    Console.WriteLine("Serveerster: brengt klant gevraagde menukaart");
                    klant.BrengMenukaart();
                    break;

                case Klant.Wens.Koffie:
                case Klant.Wens.Pannenkoek:
                    Console.WriteLine("Serveerster: neemt bestelling op, brengt daarna klant gevraagde bestelling");
                    klant.BrengBestelling();
                    break;

                case Klant.Wens.Rekening:
                    Console.WriteLine("Serveerster: brengt klant gevraagde rekening");
                    klant.BrengRekening();
                    klantVertrekt = true;
                    break;

                case Klant.Wens.Niets:
                default:
                    Console.WriteLine("Serveerster: klant heeft geen wens");
                    break;
            }
            return !klantVertrekt;
        }
    }
}
