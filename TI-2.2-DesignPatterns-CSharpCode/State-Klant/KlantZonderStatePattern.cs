﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    /// <summary>
    /// Klant klasse zonder gebruik van het State design pattern
    /// </summary>
    class KlantZonderStatePattern
    {
        public enum Wens { Niets, Menukaart, Koffie, Pannenkoek, Rekening };
        private enum Status { WachtOpMenukaart, LeesMenukaart, WachtOpBestelling, ConsumeerBestelling, WachtOpRekening, Vertrek };

        private Status status;
        private Random toevalsgetal;

        public KlantZonderStatePattern()
        {
            toevalsgetal = new Random();
            status = Status.WachtOpMenukaart;
        }

        public Wens DeelWensMee()
        {
            Wens wens = Wens.Niets; // Default is geen wens

            switch (status)
            {
                case Status.WachtOpMenukaart:
                    Console.WriteLine("Klant: vraagt menukaart");
                    wens = Wens.Menukaart;
                    break;

                case Status.LeesMenukaart:
                    if (KeuzeGemaakt())
                    {
                        Console.WriteLine("Klant: heeft keuze gemaakt");
                        status = Status.WachtOpBestelling;
                        wens = KiesBestelling();
                    }
                    else
                    {
                        Console.WriteLine("Klant: is nog niet klaar met menukaart lezen");
                    }
                    break;

                case Status.ConsumeerBestelling:
                    if (BestellingGeconsumeerd())
                    {
                        if (WilAfrekenen())
                        {
                            Console.WriteLine("Klant: wil afrekenen en vraagt de rekening");
                            status = Status.WachtOpRekening;
                            wens = Wens.Rekening;
                        } else {
                            Console.WriteLine("Klant: wil nog iets bestellen");
                            status = Status.WachtOpMenukaart;
                            wens = Wens.Menukaart;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Klant: is nog niet klaar met eten/drinken");
                    }
                    break;

                case Status.WachtOpRekening:
                case Status.WachtOpBestelling:
                case Status.Vertrek:
                default:
                    Console.WriteLine("Klant: geen wens");
                    break; // Geen wens
            }
            return wens;
        }

        public void BrengMenukaart()
        {
            if (status == Status.WachtOpMenukaart)
            {
                Console.WriteLine("Klant: ontvangt menukaart en gaat die bekijken");
                status = Status.LeesMenukaart;
            }
            else
            {
                Console.WriteLine("ERROR: Klant ontvangt menukaart in status {0}", status);
            }
        }

        public void BrengBestelling()
        {
            if (status == Status.WachtOpBestelling)
            {
                Console.WriteLine("Klant: ontvangt bestelling en gaat die consumeren");
                status = Status.ConsumeerBestelling;
            }
            else
            {
                Console.WriteLine("ERROR: Klant ontvangt bestelling in status {0}", status);
            }
        }

        public void BrengRekening()
        {
            if (status == Status.WachtOpRekening)
            {
                Console.WriteLine("Klant: ontvangt en betaalt rekening, en vertrekt");
                status = Status.Vertrek;
            }
            else
            {
                Console.WriteLine("ERROR: Rekening ontvangen in status {0}", status);
            }
        }

        protected virtual bool KeuzeGemaakt()
        {
            return (toevalsgetal.NextDouble() > 0.5);
        }

        protected virtual Wens KiesBestelling()
        {
            if (toevalsgetal.NextDouble() < 0.5) {
                return Wens.Koffie;
            }
            else
            {
                return Wens.Pannenkoek;
            }
        }

        protected virtual bool BestellingGeconsumeerd()
        {
            return (toevalsgetal.NextDouble() > 0.6);
        }

        protected virtual bool WilAfrekenen()
        {
            return (toevalsgetal.NextDouble() > 0.4);
        }
    }
}
