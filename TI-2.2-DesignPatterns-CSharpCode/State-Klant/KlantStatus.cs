﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    interface KlantStatus
    {
        Klant.Wens DeelWensMee(Klant klant);
        void BrengMenukaart(Klant klant);
        void BrengBestelling(Klant klant);
        void BrengRekening(Klant klant);
    }
}
