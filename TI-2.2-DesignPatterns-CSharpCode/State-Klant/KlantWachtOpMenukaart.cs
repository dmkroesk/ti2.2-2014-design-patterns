﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class KlantWachtOpMenukaart : KlantStatus
    {
        public Klant.Wens DeelWensMee(Klant klant)
        {
            return Klant.Wens.Menukaart;
        }

        public void BrengMenukaart(Klant klant)
        {
            Console.WriteLine("Klant: ontvangt menukaart en gaat die bekijken");
            klant.WijzigStatus(new KlantLeestMenukaart());
        }

        public void BrengBestelling(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt bestelling in status {0}", klant.VraagStatus());
        }

        public void BrengRekening(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt rekening in status {0}", klant.VraagStatus());
        }
    }
}
