﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class TestKlant
    {
        public static void TestKlantZonderStatePattern()
        {
            KlantZonderStatePattern k1 = new KlantZonderStatePattern();
            Serveerster s = new Serveerster();
            Console.WriteLine("\nTest zonder state pattern\n");
            while (s.ServeerKlantZonderStatePattern(k1))
            {
                Console.WriteLine("Test: serveer klant");
            }
            Console.WriteLine("Test: serveerster klaar met serveren, klant is vertrokken");
        }

        public static void TestKlantMetStatePattern()
        {
            Klant k1 = new Klant();
            Serveerster s = new Serveerster();
            Console.WriteLine("\nTest met state pattern\n");
            while (s.ServeerKlant(k1))
            {
                Console.WriteLine("Test: serveer klant");
            }
            Console.WriteLine("Test: serveerster klaar met serveren, klant is vertrokken");
        }
    }
}
