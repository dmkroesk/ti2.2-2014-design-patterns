﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State_Klant
{
    class KlantVertrekt : KlantStatus
    {
        public Klant.Wens DeelWensMee(Klant klant)
        {
            return Klant.Wens.Niets;
        }

        public void BrengMenukaart(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt menukaart in status {0}", klant.VraagStatus());
        }

        public void BrengBestelling(Klant klant)
        {
            Console.WriteLine("ERROR: Klant ontvangt bestelling in status {0}", klant.VraagStatus());
        }

        public void BrengRekening(Klant klant)
        {
            Console.WriteLine("ERROR: Rekening ontvangen in status {0}", klant.VraagStatus());
        }
    }
}
