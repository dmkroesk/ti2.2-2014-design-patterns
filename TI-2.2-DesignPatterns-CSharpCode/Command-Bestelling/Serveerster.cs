﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Bestelling
{
    class Serveerster
    {
        private Bedrijfsleider bedrijfsleider;
        private Bar bar;
        private Keuken keuken;
        private Kassa kassa;

        public Serveerster(Bedrijfsleider bl, Bar br, Keuken kk, Kassa ks)
        {
            bedrijfsleider = bl;
            bar = br;
            keuken = kk;
            kassa = ks;
        }

        public void VraagMenuKaart()
        {
            Console.WriteLine("Serveerster geeft menukaart aan klant"); // Direct, zonder Command pattern
        }

        public void BestelKoffie()
        {
            Console.WriteLine("Serveerster maakt bestelling voor koffie");
            Bestelling bestelling = new KoffieBestelling(bar);
            bedrijfsleider.VoegBestellingToe(bestelling);
        }

        public void BestelPannenkoek()
        {
            Console.WriteLine("Serveerster maakt bestelling voor pannenkoek");
            Bestelling bestelling = new PannenkoekBestelling(keuken);
            bedrijfsleider.VoegBestellingToe(bestelling);
        }

        public void VraagRekening()
        {
            kassa.MaakRekeningOp(); // Direct, zonder Command pattern
            Console.WriteLine("Serveerster geeft rekening aan klant");
        }
    }
}
