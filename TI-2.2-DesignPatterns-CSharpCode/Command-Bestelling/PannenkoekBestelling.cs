﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Bestelling
{
    class PannenkoekBestelling : Bestelling
    {
        private Keuken keuken;

        public PannenkoekBestelling(Keuken k)
        {
            keuken = k;
        }

        public override void HandelAf()
        {
            keuken.BakPannenkoek();
        }
    }
}
