﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Bestelling
{
    class KoffieBestelling : Bestelling
    {
        private Bar bar;

        public KoffieBestelling(Bar b)
        {
            bar = b;
        }

        public override void HandelAf()
        {
            bar.MaakKoffie();
        }
    }
}
