﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Bestelling
{
    class TestBestelling
    {
        public static void TestBestelling1()
        {
            Bedrijfsleider bedrijfsleider = new Bedrijfsleider();
            Keuken keuken = new Keuken();
            Bar bar = new Bar();
            Kassa kassa = new Kassa();
            Serveerster serveerster = new Serveerster(bedrijfsleider, bar, keuken, kassa);

            serveerster.VraagMenuKaart();

            serveerster.BestelKoffie();
            serveerster.BestelPannenkoek();
            bedrijfsleider.VerwerkAlleBestellingen();
            
            serveerster.VraagRekening();
        }
    }
}
