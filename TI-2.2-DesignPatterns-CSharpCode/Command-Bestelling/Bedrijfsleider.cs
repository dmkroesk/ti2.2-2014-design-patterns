﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Bestelling
{
    class Bedrijfsleider
    {
        private List<Bestelling> bestellingen;

        public Bedrijfsleider()
        {
            bestellingen = new List<Bestelling>();
        }

        public void VoegBestellingToe(Bestelling bestelling)
        {
            Console.WriteLine("Bedrijfsleider ontvangt bestelling");
            bestellingen.Add(bestelling);
        }

        public void VerwerkAlleBestellingen()
        {
            Console.WriteLine("--- Bedrijfsleider verwerkt alle bestellingen");
            foreach (Bestelling bestelling in bestellingen)
            {
                VerwerkBestelling(bestelling);
            }
            bestellingen.Clear();
            Console.WriteLine("--- Alle bestellingen verwerkt");
        }

        public void VerwerkBestelling(Bestelling bestelling)
        {
            bestelling.HandelAf();
        }
    }
}
